
var HOME_PAGE = 'page1';

var HOME_SLIDE = 0;
var MENU_SLIDE = 3;
var SECTION_1 = 3;
var SECTION_2 = 9;
var SECTION_3 = 13;
var SECTION_4 = 17;
var FINAL_SLIDE = 20;

//adding the action to the button
$(document).on('click', '#homeButton', function() {
  if(lockNavigation)  {
    return;
  }
  $('#homeNavManipulationSlide').addClass('set-1-nav');
  $('#homeNavManipulationSlide').removeClass('slide-2');
  $.fn.fullpage.silentMoveTo(HOME_PAGE, HOME_SLIDE+1);
  lockNavigation = true;
  $.fn.fullpage.moveTo(HOME_PAGE, HOME_SLIDE);
  setTimeout(function() {
    $('#homeNavManipulationSlide').addClass('slide-2');
    $('#homeNavManipulationSlide').removeClass('set-1-nav');
  }, 1000);
});
$(document).on('click', '#menuButton', function() {
  if(lockNavigation)  {
    return;
  }
  var newClass = $('.slide.active').attr('data-anchor');
  $('#navNavManipulationSlide').removeClass('slide-5');
  $('#navNavManipulationSlide').addClass(newClass);
  $.fn.fullpage.silentMoveTo(HOME_PAGE, MENU_SLIDE+1);
  lockNavigation = true;
  $.fn.fullpage.moveTo(HOME_PAGE, MENU_SLIDE);
  setTimeout(function() {
    $('#navNavManipulationSlide').addClass('slide-5');
    $('#navNavManipulationSlide').removeClass(newClass);
  }, 1000);
});
$(document).on('click', '#section1Button', function() {
  if(lockNavigation)  {
    return;
  }
  $.fn.fullpage.silentMoveTo(HOME_PAGE, SECTION_1);
  $.fn.fullpage.moveTo(HOME_PAGE, SECTION_1+1);
});
$(document).on('click', '#section2Button', function() {
  if(lockNavigation)  {
    return;
  }
  $.fn.fullpage.silentMoveTo(HOME_PAGE, SECTION_2);
  $.fn.fullpage.moveTo(HOME_PAGE, SECTION_2+1);
});
$(document).on('click', '#section3Button', function() {
  if(lockNavigation)  {
    return;
  }
  $.fn.fullpage.silentMoveTo(HOME_PAGE, SECTION_3);
  $.fn.fullpage.moveTo(HOME_PAGE, SECTION_3+1);
});
$(document).on('click', '#section4Button', function() {
  if(lockNavigation)  {
    return;
  }
  $.fn.fullpage.silentMoveTo(HOME_PAGE, SECTION_4);
  $.fn.fullpage.moveTo(HOME_PAGE, SECTION_4+1);
});

function showPrevArrow() {
  $( ".fp-prev" ).removeClass('hide-nav');
}

function hidePrevArrow() {
  $(".fp-prev").addClass('hide-nav');
}

function showNextArrow() {
  $( ".fp-next" ).removeClass('hide-nav');
}

function hideNextArrow() {
  $(".fp-next").addClass('hide-nav');
}

function showHomeButton() {
  $( "#homeButton" ).removeClass('hide-nav');
}

function hideHomeButton() {
  $( "#homeButton" ).addClass('hide-nav');
}

function showMenuButton() {
  $( "#menuButton" ).removeClass('hide-nav');
}

function hideMenuButton() {
  $( "#menuButton" ).addClass('hide-nav');
}

function toggleNavigationButtons(anchorLink, index, slideIndex, direction, nextSlideIndex, leavingSlide)  {
  menuAndHomeDisplay(nextSlideIndex);
  prevArrowDisplay(nextSlideIndex);
  nextArrowDisplay(nextSlideIndex);
}

function menuAndHomeDisplay(nextIndex) {
  if(nextIndex == MENU_SLIDE) {
    hideMenuButton();
    showHomeButton();
  } else {
    hideHomeButton();
    showMenuButton();
  }
}

function nextArrowDisplay(nextIndex)  {
  if(isContactpage(nextIndex) || isNavpage(nextIndex)) {
    hideNextArrow();
  } else {
    showNextArrow();
  }
}

function prevArrowDisplay(nextIndex)  {
  if(nextIndex == HOME_SLIDE || isNavpage(nextIndex) || isSetFirstPage(nextIndex)) {
    hidePrevArrow();
  } else {
    showPrevArrow();
  }
}

function toggleMailToLink(nextIndex)  {
  if(isContactpage(nextIndex))  {
    displayMailTo(nextIndex);
  } else {
    hideMailto(nextIndex);
  }
}

function isNavpage(nextSlideIndex) {
  return (nextSlideIndex == MENU_SLIDE || nextSlideIndex == SECTION_1 || nextSlideIndex == SECTION_2 || nextSlideIndex == SECTION_3 || nextSlideIndex == SECTION_4)
}

function isSetFirstPage(nextSlideIndex) {
  return (nextSlideIndex == SECTION_1 + 1 || nextSlideIndex == SECTION_2 + 1 || nextSlideIndex == SECTION_3 + 1 || nextSlideIndex == SECTION_4 + 1)
}

function isContactpage(nextSlideIndex) {
  return (nextSlideIndex == SECTION_2-1 || nextSlideIndex == SECTION_3-1 || nextSlideIndex == SECTION_4-1 || nextSlideIndex == FINAL_SLIDE)
}

function checkLeftFromSetFour(slideIndex, direction, nextSlideIndex) {
  return slideIndex == SECTION_4 && direction == 'left' && nextSlideIndex == SECTION_4-1;
}

function checkLeftFromSetThree(slideIndex, direction, nextSlideIndex) {
  return slideIndex == SECTION_3 && direction == 'left' && nextSlideIndex == SECTION_3-1;
}

function checkLeftFromSetTwo(slideIndex, direction, nextSlideIndex) {
  return slideIndex == SECTION_2 && direction == 'left' && nextSlideIndex == SECTION_2-1;
}

function checkLeftFromSetOne(slideIndex, direction, nextSlideIndex) {
  return slideIndex == SECTIOM_1 && direction == 'left' && nextSlideIndex == SECTIOM_1-1;
}

function displayMailTo() {
  var element = document.getElementById('mailto').style.display = 'block'
}

function hideMailto() {
  var element = document.getElementById('mailto').style.display = 'none'
}
